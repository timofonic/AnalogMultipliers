Version 4
SHEET 1 1700 932
WIRE 0 0 -48 0
WIRE 144 0 48 0
WIRE 192 0 144 0
WIRE -48 32 -48 0
WIRE 48 32 48 0
WIRE 144 32 144 0
WIRE -48 128 -48 112
WIRE 48 128 48 112
WIRE 144 144 144 112
WIRE 560 144 144 144
WIRE 784 144 560 144
WIRE 960 144 784 144
WIRE 144 176 144 144
WIRE 304 208 256 208
WIRE 400 208 384 208
WIRE 416 208 400 208
WIRE 560 208 560 144
WIRE 560 208 496 208
WIRE 256 224 256 208
WIRE 560 240 560 208
WIRE 784 240 784 144
WIRE 960 240 960 144
WIRE 144 256 144 240
WIRE 256 352 240 352
WIRE 304 352 256 352
WIRE 416 352 384 352
WIRE 560 352 560 320
WIRE 560 352 496 352
WIRE 784 368 784 320
WIRE 1024 368 784 368
WIRE 1136 368 1104 368
WIRE 1168 368 1136 368
WIRE 1280 368 1248 368
WIRE 1280 384 1280 368
WIRE 256 416 256 352
WIRE 416 448 416 352
WIRE 432 448 416 448
WIRE 560 448 560 352
WIRE 512 464 496 464
WIRE 1136 464 1136 368
WIRE 1232 464 1136 464
WIRE 400 480 400 208
WIRE 432 480 400 480
WIRE 1312 480 1296 480
WIRE 1232 496 1136 496
WIRE 256 512 256 496
WIRE 960 560 960 320
WIRE 1024 560 960 560
WIRE 1136 560 1136 496
WIRE 1136 560 1104 560
WIRE 1168 560 1136 560
WIRE 1312 560 1312 480
WIRE 1312 560 1248 560
WIRE 1344 560 1312 560
WIRE 560 576 560 544
WIRE 656 576 560 576
WIRE 560 608 560 576
WIRE 784 608 784 368
WIRE 960 608 960 560
WIRE 656 656 656 576
WIRE 656 656 624 656
WIRE 720 656 656 656
WIRE 896 656 720 656
WIRE 1216 656 1120 656
WIRE 1120 672 1120 656
WIRE 496 688 368 688
WIRE 560 720 560 704
WIRE 960 720 960 704
WIRE 368 752 368 688
WIRE 400 752 368 752
WIRE 496 768 496 688
WIRE 496 768 464 768
WIRE 784 768 784 704
WIRE 784 768 496 768
WIRE 208 784 192 784
WIRE 272 784 208 784
WIRE 368 784 352 784
WIRE 400 784 368 784
WIRE 1056 784 960 784
WIRE 192 816 192 784
WIRE 368 816 368 784
WIRE 960 816 960 784
WIRE 192 912 192 896
WIRE 368 912 368 896
FLAG 48 128 0
FLAG 192 0 V+
FLAG 144 256 0
FLAG 256 224 0
FLAG 464 432 V+
FLAG 464 496 V-
FLAG -48 128 0
FLAG 0 0 V-
FLAG 256 512 0
FLAG 240 352 A
FLAG 560 720 0
FLAG 960 720 0
FLAG 432 736 V+
FLAG 432 800 V-
FLAG 368 912 0
FLAG 192 912 0
FLAG 1280 384 0
FLAG 1264 512 V+
FLAG 1264 448 V-
FLAG 1344 560 out
FLAG 1120 752 0
FLAG 1216 656 thermal
FLAG 960 896 0
FLAG 1056 784 ideal
FLAG 208 784 B
SYMBOL voltage 48 16 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value 15
SYMBOL res 128 16 R0
SYMATTR InstName R1
SYMATTR Value 330
SYMBOL zener 160 240 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D1
SYMATTR Value BZX84C6V8
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL res 544 224 R0
SYMATTR InstName R2
SYMATTR Value {R4}
SYMBOL res 400 192 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 1Meg
SYMBOL res 512 192 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 1Meg
SYMBOL res 400 336 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R5
SYMATTR Value 1Meg
SYMBOL res 512 336 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R6
SYMATTR Value 1Meg
SYMBOL pmos 512 544 M180
SYMATTR InstName M1
SYMATTR Value BSS84
SYMBOL Opamps\\TI\\TL072 464 464 R0
WINDOW 0 27 -53 Left 2
WINDOW 3 24 -26 Left 2
SYMATTR InstName U1
SYMBOL voltage -48 16 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value -15
SYMBOL voltage 256 400 M0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V3
SYMATTR Value SINE(.5 .5 2k 0 0 270)
SYMBOL npn 624 608 M0
SYMATTR InstName Q1
SYMATTR Value CA3096N
SYMBOL npn 720 608 R0
SYMATTR InstName Q2
SYMATTR Value CA3096N
SYMBOL res 768 224 R0
SYMATTR InstName R7
SYMATTR Value {R3}
SYMBOL npn 896 608 R0
SYMATTR InstName Q3
SYMATTR Value CA3096N
SYMBOL res 944 224 R0
SYMATTR InstName R8
SYMATTR Value {R3}
SYMBOL Opamps\\LT\\LT1013 432 704 R0
SYMATTR InstName U2
SYMBOL res 368 768 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R9
SYMATTR Value {R1}
SYMBOL res 352 800 R0
SYMATTR InstName R10
SYMATTR Value {R2}
SYMBOL voltage 192 800 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V4
SYMATTR Value SINE(0 1 10k)
SYMBOL res 1120 352 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R11
SYMATTR Value {R5}
SYMBOL res 1120 544 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R12
SYMATTR Value {R5}
SYMBOL res 1264 352 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R13
SYMATTR Value {R6}
SYMBOL res 1264 544 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R14
SYMATTR Value {R6}
SYMBOL Opamps\\TI\\TL072 1264 480 M180
WINDOW 0 25 49 Left 2
WINDOW 3 20 24 Left 2
SYMATTR InstName U3
SYMBOL bv 1120 656 R0
SYMATTR InstName B1
SYMATTR Value V=86.2u*(temp+273.15)
SYMBOL bv 960 800 R0
SYMATTR InstName B2
SYMATTR Value V=({R3/R4}/V(thermal))*{R2/(R1+R2)}*{R6/R5}*V(B)*V(A)
TEXT 48 -48 Left 2 !.lib diode.lib
TEXT 48 -72 Left 2 !.tran 1m
TEXT 48 -24 Left 2 !.lib bipolar.lib
TEXT 504 -32 Left 2 !.param R1=10k\n.param R2=20\n.param R3=1k\n.param R4=1k\n.param R5=430k\n.param R6=5.6Meg
