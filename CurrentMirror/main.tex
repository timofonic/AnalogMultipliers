\documentclass{article}

\usepackage[letterpaper,margin=2.5cm]{geometry}
\usepackage{color}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{libertine}
\usepackage{euler}
\usepackage{graphicx}
\usepackage{framed}

\setlength{\parindent}{0mm}

\newcommand{\ra}{\ensuremath{\rightarrow}}
\newcommand{\mr}[1]{\ensuremath{\mathrm{#1}}}

\newcommand{\note}[1]{ %\
 \begin{framed} %\
 #1 %\
 \end{framed} }

\title{Current Mirror Analog Multiplier}
\author{Chris Pavlina}
\date{\today}

\begin{document}

\maketitle

\begin{abstract}
    Analog multipliers are circuits that multiply two signals $A$ and $B$
    to provide an output $kAB$. This simple, and unfortunately poor,
    multiplier is the first in a short series of analog multiplier
    experiments. It is a \emph{two-quadrant} analog multiplier (meaning
    that one input is restricted to positive values), operating by
    injecting a tiny signal into a current mirror.
\end{abstract}


\tableofcontents
\setlength{\parskip}{4mm}
\section{Theoretical: Designing the simplified circuit}

\subsection{Introduction: Small- and large-signal models}

There are two model types that are typically used to design transistor
circuits: the full \emph{large-signal model} and the simplified
\emph{small-signal model}. This is due to the fact that transistors are not
linear; if we are dealing with very small signals, we can simplify the
generally undesirable curves of the large-signal model into lines that are
tangent to them.

This is the basic Ebers-Moll equation describing the operation of a bipolar
junction transistor (BJT):

\begin{equation}\label{ebersmoll}
I_C = I_S\left( e^{\frac{V_{BE}}{V_T}} - 1 \right)
\end{equation}

$I_C$ is the current flowing into the transistor's collector, and $V_{BE}$ is
the voltage applied between the base and the emitter. $I_S$ is the reverse
saturation current of the device, which is exponentially related to temperature,
and $V_T$ is the thermal voltage, which is proportional to absolute temperature.

The small-signal model is simply a line with a specified slope. We use the notation
$i_C$ (note the lower-case $i$) for the tiny variations in collector current, the
notation $v_{BE}$ (again, lower-case $v$) for the tiny variations in base-emitter
voltage, and the notation $g_m$ for the slope of the line. This slope is
called \emph{transconductance}, and has units of \emph{siemens} (S) or
amperes per volt ($\mr{A}\cdot\mr{V}^{-1}$).

\begin{equation}\label{smallsignal}
i_C = v_{BE} \;\cdot\; g_m
\end{equation}

Remember that the transconductance is just the slope of the large-signal model
at a given point, so it is actually a partial derivative:

\begin{equation}\label{gm-partial}
g_m = \frac{\partial}{\partial V_{BE}} I_C = \frac{I_S}{V_T} e^{\frac{V_{BE}}{V_T}}
\end{equation}

If we're willing to make a tiny approximation, this can be simplified. For any
practical values of $V_{BE}$, $e^\frac{V_{BE}}{V_T}$ will be \emph{huge} (on the order
of $10^9$). This means that $e^\frac{V_{BE}}{V_T} \approx e^\frac{V_{BE}}{V_T} - 1$.
We can pull this from the Ebers-Moll equation \eqref{ebersmoll} as just
$\frac{I_C}{I_S}$:

\begin{equation}\label{smallsignal-transconductance-simp}
g_m = \frac{I_C}{V_T}
\end{equation}

Here is a plot showing the large-signal Ebers-Moll model of a transistor
($I_S = 1\;\mr{pA}$, $V_T = 26\;\mr{mV}$), and two small-signal models at
different points:

\begin{center}
\includegraphics[width=3in]{concept-exp}
\end{center}

\subsection{The concept}

We could use one transistor as a sort of multiplier. If one input (we'll call
it $V_A$) is a small-signal variation applied to the transistor's base, then
it will be multiplied by $g_m$. We just need a way to make $g_m$ a function of
the second input $V_B$, so that the output will be proportional to
$V_A V_B$. That's easy enough, since $g_m$ is proportional to the large-signal
collctor current.

\begin{center}
\includegraphics[width=2in]{bjt-smallsignal}
\end{center}

Suppose $V_{BE}$ were chosen to give exactly a certain $I_C$, which is
proportional to $V_B$. This means that $g_m$ is also proportional to
$V_B$. Now, we just inject a small signal $v_{BE}$ which is proportional
to $V_A$, giving $i_C \propto g_m V_A \propto V_B V_A$.

\subsection{Setting $V_{BE}$, $I_C$, and $g_m$ using a current mirror}

A simple way to get this conveniently selected $V_{BE}$ is the traditional
\emph{current mirror} configuration:

\begin{center}
\includegraphics[width=3in]{currentmirror}
\end{center}

The input voltage $V_B$ feeds into a voltage-controlled current source with
transconductance $G$, giving a collector current $I_C$ equal to $GV_B$.
This current flows into the first transistor, requiring it to
develop a certain base-emitter voltage, $V_{BE}$. This same voltage is applied
to the second transistor. If the two transistors are identical, so that all
the properties in the Ebers-Moll equation \eqref{ebersmoll} are equal, this
will cause an equal collector current in the second transistor.

The resistor is not a required component of the mirror itself, but it allows
us to detect what the collector current actually is, by converting it to a
voltage via Ohm's Law.

\subsection{Injecting the small-signal $v_{BE}$}

Now that we have set $g_m$ by way of $I_C$ (thereby providing $V_B$ to the
circuit), we have to provide $V_A$ by adding it to the base-emitter voltage.
The first, immediately obvious way to do this is to put the small-signal $v_a$
in series with the base (note lower-case $a$; this is a tiny voltage
proportional to $V_A$):

\begin{center}
\includegraphics[width=3in]{multiplier-step1}
\end{center}

This is a bit impractical, however. Another way to inject a signal into the
base-emitter voltage is to move the \emph{emitter} by an opposite voltage:

\begin{center}
\includegraphics[width=3in]{multiplier-step2}
\end{center}

\subsection{Removing the unwanted $I_C$ term}

This gives a collector current of $I_C + g_m v_a$ or $GV_B + g_m v_a$.
The extra term in the
expression, $GV_B$, cannot be tolerated as an error term, because it will be
significant relative to the desired $g_m v_a$. Removing it is simple,
though: we can simply add a \emph{third} branch to the current mirror to
produce \emph{only} $GV_B$, and then subtract the two using a differential
amplifier.

\begin{center}
\includegraphics[width=3in]{multiplier-step3}
\end{center}

This has the added benefit of removing the need to invert $v_a$, as we can
just swap the inputs to the differential amplifier to undo any undesired
inversion inherent in the multiplier.

\subsection{Transfer function}

We now have a working multiplier, though it has a few problems that must be
solved. The output of the device is currently equal to $R g_m v_a$. Remember
that $g_m = \frac{I_C}{V_T}$, and $I_C = GV_B$. We now have the final transfer
function of our prototype multiplier:

\begin{equation}\label{proto-txfs3}
V_\mr{out} = R g_m v_a = \frac{RG}{V_T} V_B v_a
\end{equation}



\section{Theoretical: Making it practical}
\subsection{Converting $V_A$ to $v_a$}

The larger our $v_a$, the more error we will have due to the imprecision
of the small-signal model. If we make it \emph{too} small, however, we'll
encounter a large number of other issues (signals will be small compared
to noise voltages and offset voltages). I'll choose a ``happy medium'' of
$4\;\mr{mV_{p-p}}$, and a maximum input voltage of $2\;\mr{V_{p-p}}$ or
$1\;\mr{V_{pk}}$ for mathematical simplicity. This gives a division
ratio of 500.

One option for generating this would be to use a simple voltage divider with
a low Th\'evenin output impedance and connect this directly as $v_a$. However,
even this low impedance would add \emph{some} emitter degeneration to the
circuit, and unlike in linear circuits, we want the transfer function to
remain as close to exponential as possible by driving the input using a
\emph{very} low impedance.

The answer is simple: we can divide the voltage, and then use a precision
op-amp buffer to drive $v_a$.

\begin{center}
\includegraphics[width=3in]{multiplier-step4}
\end{center}

We can update equation \eqref{proto-txfs3} to give the new transfer
function:

\begin{equation}\label{proto-txfs4}
    V_\mr{out} = \left(\frac{R_3 G}{V_T}\right) \left(\frac{R_2}{R_1+R_2}\right) V_B V_A
\end{equation}

\subsection{A practical current source for $GV_B$}

In order to use this circuit for DC multiplication work and not just simple
amplitude modulation, we want $V_B$ to be able to go near zero. This will
require a more accurate current source than we could construct with just a
few transistors; we'll use another op-amp:

\begin{center}
\includegraphics[width=5in]{multiplier-step5}
\end{center}

\note{It might look as though the two voltages, indicated by the grey arrows
    and ``see note'' above, would be equal, entirely removing the need for the
    third branch of the current mirror. However, note that the leftmost transistor
    of the current mirror has a significantly different collector---base voltage
    $V_{CB}$ than the other two transistors. This changes the current gain
    $\beta$ due to the Early effect, meaning the currents through the mirror's
    output transistors are slightly higher than $\frac{V_B}{R_4}$. A third
    transistor will suffer the same effect, maintaining error voltage equal
between the two.}

We will update equation \eqref{proto-txfs4}:

\begin{equation}\label{proto-txfs5}
    V_\mr{out} = \left(\frac{R_3}{R_4 V_T}\right) \left(\frac{R_2}{R_1+R_2}\right) V_B V_A
\end{equation}

\subsection{A practical output amplifier}

The idealized differential amplifier can be replaced by a real one, constructed
using an op-amp and four resistors:

\begin{center}
\includegraphics[width=5in]{multiplier-step6}
\end{center}

This differential amplifier can have gains other than unity, so we update
the transfer function in equation \eqref{proto-txfs5} to include the new gain:

\begin{equation}\label{proto-txfs6}
V_\mr{out} = \left(\frac{R_3}{R_4 V_T}\right)
    \left( \frac{R_2}{R_1+R_2} \right)
    \left( \frac{R_6}{R_5} \right) V_B V_A
\end{equation}

\subsection{Voltage regulation}

The positive rail in this circuit should operate at a fixed voltage, because
variation in this voltage will cause gain error due to the Early effect.
Additionally, it should be below the positive rail supplying the op-amps,
unless they are rail-to-rail types, because the voltages that they must sense
are close to the positive rail. We'll use a Zener diode to provide a $6.8\;\mr{V}$
rail:

\begin{center}
\includegraphics[width=5in]{multiplier-step7}
\end{center}

\subsection{Setting the gains}

Now that the full circuit has been designed, we need to choose values for
$R_1$, $R_2$, $R_5$, and $R_6$ to determine the system gain. A desirable
transfer function, due to its simplicity, could be:

\begin{equation}\label{txf-desirable}
V_\mr{out} = \left(1\;\mr{V}^{-1}\right) V_A V_B
\end{equation}

Let's see the full transfer function, again:
\begin{equation}
V_\mr{out} = \left(\frac{R_3}{R_4 V_T}\right)
    \left( \frac{R_2}{R_1+R_2} \right)
    \left( \frac{R_6}{R_5} \right) V_B V_A
\tag{\ref{proto-txfs6} revisited}
\end{equation}

The small-signal $v_a$
needs to be tiny; we already decided that it should be approximately $\tfrac{1}{500} V_A$.
The nearest pair of standard E24 resistors that gives this ratio is $10\;\mr{k\Omega}$ and
$20\;\mr{\Omega}$, which gives a ratio of $\tfrac{1}{501}$.

This version of the circuit is not temperature compensated, so we'll assume
$V_T$ to be its room-temperature value of $26\;\mr{mV}$. Now that we have selected
$R_1$ and $R_2$, and $R_3$ and $R_4$ have already been chosen to be equal for
simplicity, we can solve for the ratio of $R_5$ and $R_6$:

\begin{equation}
1\;\mr{V}^{-1} = \left(\frac{1}{26\;\mr{mV}}\right)
\left(\frac{20\;\mr\Omega}{10\;\mr{k\Omega} + 20\;\mr\Omega}\right)
\left(\frac{R_6}{R_5}\right) \\
\end{equation}

\begin{equation}
    \frac{R_6}{R_5} = 13.026
\end{equation}

A nearby pair of E24 resistors is $R_5 = 430\;\mr{k\Omega}$ and $R_6 = 5.6\;\mr{M\Omega}$,
with a ratio of $13.023$.

\section{Building the circuit}
\subsection{Parts}

To fully test this circuit, we need to select parts. The following selections were made:

\begin{itemize}
\item{Current source op-amp: one half of a TL072: JFET op-amp, because large
    resistors require high-impedance inputs}
\item{Output op-amp: one half of a TL072: JFET op-amp, because large resistors
    require high-impedance inputs}
\item{$v_a$ op-amp: OP07C: precision op-amp, because even a small offset will
    cause a relatively large error}
\item{Current mirror matched BJTs: CA3096: LM3046 would be preferable as it is
    still in production, but none were on hand}
\item{Current source P-FET: a comically oversized IRF9540, as it was the only
    P-FET on hand that would fit in a breadboard}
\item{Power supply: the circuit was provided $+15\;\mr{V}$ and $-15\;\mr{V}$.}
\end{itemize}

\subsection{Amplitude modulation demo}

The circuit was built on a breadboard and supplied with two signals:

\begin{gather}
V_A = (1\;\mr V) \sin(2\pi(10\;\mr{kHz})t) \\
V_B = 0.5\;\mr V + (0.5\;\mr V) \sin(2\pi(2\;\mr{kHz})t)
\end{gather}


\begin{center}
\includegraphics[width=4in]{testing/test-am}
\end{center}


It is very sensitive to imbalance between the $1\;\mr{k\Omega}$ resistors in the
current mirror; one of them was replaced by a $2\;\mr{k\Omega}$ potentiometer to
allow the circuit to be balanced.

\subsection{Characterization}

A dual-channel function generator (generating DC) and a multimeter were scripted
to test the multiplier over its full range, over almost 2600 points. A quick plot
of the data shows that the device indeed works as a DC multiplier:

\begin{center}
\includegraphics[width=4in]{testing/transfer}
\end{center}

Unfortunately, choosing a value for $V_B$, plotting $V_A$ vs. the output, and
comparing to a linear fit shows a different picture, though:

\begin{center}
\includegraphics[width=4in]{testing/transfer-a48}
\end{center}

The multiplier certainly \emph{works} as a multiplier, but it shows significant
deviation from linearity --- yes, the axis scale is correct!

\begin{center}
\includegraphics[width=4in]{testing/nonlinearity}
\end{center}

\section{Conclusion}

With linearity deviation up to $55\%$ at certain values and no
temperature compensation, this is not a
particularly good DC multiplier, though it could be used for AC modulation
if restricted to its ``good'' regions of operation. It is also one of the
simplest multipliers we'll look at, able to be built from a small collection
of relatively generic parts.


\end{document}
