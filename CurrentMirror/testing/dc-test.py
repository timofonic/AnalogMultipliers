#!/usr/bin/env python

import sys
import time

sys.path.append ("/home/cmp/electronics/GPIB")
import lab.tds380
import lab.dg1022

# Connect DG1022 ch1 to input A, ch2 to input B, TDS380 ch1 to output

scope = lab.tds380.tds380 ()
siggen = lab.dg1022.dg1022 ()

def conf_scope (scope):
    scope.trigMode = 'NORMAL'
    scope.trigSource = 'EXT'
    scope.acqMode = 'SAMPLE'
    scope.acqRun = False
    scope.acqSingle = True
    scope.ch1_coupling = 'DC'
    scope.ch1_scale = 0.25
    scope.horiz_scale = 0.001

def meas_dc (scope):
    scope.acqRun = True
    scope.trigger ()
    return scope.measure ('ch1', 'mean')

def conf_siggen (siggen):
    siggen.load (1, None)
    time.sleep (0.1)
    siggen.load (2, None)
    time.sleep (0.1)
    siggen.apply (1, 'dc', offs=0.0)
    time.sleep (0.1)
    siggen.apply (2, 'dc', offs=0.0)
    time.sleep (0.1)
    siggen.toggle (1, True)
    time.sleep (0.1)
    siggen.toggle (2, True)
    time.sleep (0.1)

conf_scope (scope)
conf_siggen (siggen)

inA_range = [i/25.0 for i in range (-25, 26)]
inB_range = [i/50.0 for i in range (0, 51)]

with open ('test-dc-out', 'w') as f:
    for a in inA_range:
        for b in inB_range:
            siggen.apply (1, 'dc', offs=a)
            time.sleep (0.1)
            siggen.apply (2, 'dc', offs=b)
            time.sleep (0.1)
            meas = meas_dc (scope)
            print ('%f, %f, %f' % (a, b, meas))
            f.write ('%f, %f, %f\n' % (a, b, meas))
            f.flush ()
