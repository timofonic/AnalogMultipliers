from pylab import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
rc ('font', **{'family':'sans-serif', 'sans-serif':'Linux Biolinum', 'size':16})

def float_eq (a, b):
    return abs(a - b) < min (1e-3 * a, 1e-3 * b)

# InA, InB, out
data = []

with open ("test-dc-out") as f:
    for line in f:
        ina, inb, out = line.split ()
        ina = float (ina.rstrip(','))
        inb = float (inb.rstrip(','))
        out = float (out.rstrip(','))
        data.append ((ina, inb, out))

#######################################
# Plot: output vs. InA over six InBs
inbs = sorted (set (i[1] for i in data))
inbs_plot = inbs[::len(inbs)//5]
for inb in inbs_plot:
    plot_x = [i[0] for i in data if i[1] == inb]
    plot_y = [i[2] for i in data if i[1] == inb]

    # Generate a text label on the line
    # First, find the rotation angle for it
    coeffs = polyfit (plot_x, plot_y, 1)
    angle = arctan (coeffs[0]) * (180/pi) * 0.8

    # Now, the position
    text_x = 0.6
    text_y = plot_y[16*len(plot_y)//20] * 1.45 + 0.023

    text (text_x, text_y, 'InB=%.2f V' % inb, rotation=angle)
    plot (plot_x, plot_y, 'b')

xlabel ("Input A (V)")
ylabel ("Multiplier output (V)")
title ("Multiplier Transfer Characteristic")
grid ()
savefig ('transfer.eps')
show ()

#######################################
# Plot: output vs. InB when InA = 0.48
inbs = []
outs = []
for ina, inb, out in data:
    if ina > 0.475 and ina < 0.485:
        inbs.append (inb)
        outs.append (out)

coeffs = polyfit (inbs, outs, 1)
outs_ideal = [polyval (coeffs, i) for i in inbs]

plot (inbs, outs, 'r', label="Measured output")
plot (inbs, outs_ideal, 'b', label="Ideal output")
xlabel ("Input B (V)")
ylabel ("Multiplier output (V)")
title ("Transfer Curve and Ideal Response (Input A = 0.48V)")
grid ()
legend (loc=2)
savefig ('transfer-a48.eps')
show ()


#######################################
# Plot: output vs. InA over six InBs
inbs = sorted (set (i[1] for i in data))
inbs_plot = inbs[::len(inbs)//5]
plot_args = []
inbs = sorted (list (set (i[1] for i in data if i[1] > 0.05)))
worst_errors = []

for v_inb in inbs:
    v_ina = [i[0] for i in data if i[1] == v_inb and abs(i[0]+0.08)>0.05]
    v_out = [i[2] for i in data if i[1] == v_inb and abs(i[0]+0.08)>0.05]
    coeffs = polyfit (v_ina, v_out, 1)

    v_out_ideal = [polyval(coeffs, i) for i in v_ina]
    
    errors = [(i - j)/j for i, j in zip (v_out_ideal, v_out)]
    worst_error = 100. * max (abs (i) for i in errors)
    worst_errors.append (worst_error)

plot (inbs, worst_errors)
grid ()
xlabel ("Input B (V)")
ylabel ("Output error from ideal (%)")
title ("Worst-case nonlinearity vs. InB")
savefig ('nonlinearity.eps')
show ()
