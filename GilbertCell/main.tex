\documentclass{article}

\usepackage[letterpaper,margin=2.5cm]{geometry}
\usepackage{color}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{libertine}
\usepackage{euler}
\usepackage{graphicx}
\usepackage{framed}

\setlength{\parindent}{0mm}

\newcommand{\ra}{\ensuremath{\rightarrow}}
\newcommand{\mr}[1]{\ensuremath{\mathrm{#1}}}

\newcommand{\kOhm}{\ensuremath{\;\mathrm{k\Omega}}}
\newcommand{\Ohm}{\ensuremath{\;\mathrm{\Omega}}}
\newcommand{\kHz}{\ensuremath{\;\mathrm{kHz}}}
\newcommand{\Vpp}{\ensuremath{\;\mathrm{V_{p-p}}}}

\newcommand{\note}[1]{ %\
 \begin{framed} %\
 #1 %\
 \end{framed} }

\title{Gilbert Cell Analog Multiplier}
\author{Chris Pavlina}
\date{\today}

\begin{document}

\maketitle

\begin{abstract}
    Analog multipliers are circuits that multiply two signals $A$ and $B$
    to provide an output $kAB$. This is a multiplier using the classic
    Gilbert Cell circuit, around which almost every commercial analog
    multiplier is built. This is a full four-quadrant multiplier operating
    by the Translinear Principle, with ideally high linearity and no
    temperature dependence.
\end{abstract}


\tableofcontents
\setlength{\parskip}{4mm}
\section{Introduction: The Translinear Principle}

The \emph{Translinear Principle} (TLP) \footnote{The TLP was proposed in
    1974 by Barrie Gilbert, whose name you will note bears a striking
    resemblance to the term \emph{Gilbert Cell}.}
is an extension of Kirchhoff's voltage law for devices, such as bipolar
transistors, that exhibit an exponential relationship between voltage
and current. The TLP operates on \emph{voltage loops}. This is the basic
circuit for the Gilbert Cell, with one such loop highlighted:

\begin{center}
\includegraphics[width=4in]{gilbertcell-loop}
\end{center}

To apply the TLP, we declare the P-N junctions (such as the base-emitter
junction) to be either \emph{clockwise} or \emph{counterclockwise} depending
on which way they point around the loop. In the above diagram, these are
labeled `CW' and `CCW', respectively. It is a requirement to use the TLP that
there are an equal number of clockwise junctions and counterclockwise
junctions.

Kirchhoff's voltage law states that the sum of all clockwise voltages and
the sum of all counterclockwise voltages around the loop must be equal.
\footnote{Kirchhoff's law is usually stated differently, considering ``counterclockwise''
    voltages to instead be negative. We're using CW and CCW here to maintain consistency
    with the translinear principle.}
This is more or less common sense: starting at a point, and moving around
the loop back to that point, one must return to the same voltage, so every
volt in the positive direction must cancel with a volt in the negative
direction. Formally, where $V$ is a pure, non-negative magnitude:

\begin{equation}\label{kvl}
\sum \left\{ V \mid V \textrm{ is clockwise} \right\} = \sum \left\{ V \mid V \textrm{ is counterclockwise} \right\}
\end{equation}

This has an interesting implication when combined with the exponential
voltage-current relationship of the transistor:

\begin{equation}\label{em-simp}
I_E = I_S e^{\frac{V_{BE}}{V_T}}
\end{equation}

\note{Note that this is a \emph{simplified form} of the Ebers-Moll model, using
    two simplifications that we will continue to use throughout:

    \begin{itemize}
        \item{$e^{\frac{V_{BE}}{V_T}} \approx e^{\frac{V_{BE}}{V_T}} - 1$. We can make this
                approximation because at any reasonable collector current,
                $e^{\frac{V_{BE}}{V_T}}$ is extremely large relative to $1$.}
        \item{$I_C \approx I_E$. We can make this approximation because we will simply turn
                up our noses at any transistors with insufficient current gain ($\beta$)!}
    \end{itemize}}

If $I_S$ and $V_T$ are equal for all transistors involved (this requires them to
have the same material composition, dimensions, and temperature --- in essence, to be
together on one chip), we can treat them as constants, giving us the same relationship
between current and voltage for all transistors.

Reversing this relationship makes it logarithmic, and logarithms have an incredibly
useful property:

\begin{equation}\label{logs}
\log(a) + \log(b) = \log(a\cdot b)
\end{equation}

This means that if we can provide a signal to a translinear circuit as a current, then
the addition of voltages guaranteed by Kirchhoff's voltage law translates to a
\emph{multiplication} of currents:

\begin{equation}\label{tlp}
\prod \left\{ I \mid I \textrm{ is clockwise} \right\} = \prod \left\{ I \mid I \textrm{ is counterclockwise} \right\}
\end{equation}

This is the Translinear Principle. While it might not be immediately obvious
how to take advantage of it to design a circuit, analysing a circuit with it is
quite straightforward as it often becomes just simple algebra.

\section{Theoretical: The Gilbert Cell}

The \emph{full} Gilbert cell
\footnote{The term ``Gilbert cell'' is often used for an older version invented
    --- and patented --- before Gilbert's version, which does not follow the
    translinear principle and suffers from substantial distortion. This version
    is still somewhat common, as it is simpler, and in RF applications the
    distortion can usually be filtered out.}
is shown below:

\begin{center}
\includegraphics[width=4in]{gilbertcell}
\end{center}

There are two input signals, provided as currents: $I_X$ and $I_Y$. To maintain
the transistors in the active region of operation at all times, these currents
are superimposed differentially (added to one side and subtracted from the
other) on two fixed bias currents, $I_{BX}$ and $I_{BY}$. The circuit then
pulls down two output currents, $I_{OP}$ and $I_{ON}$, where the final output
signal is considered to be $I_{OP} - I_{ON}$.

To analyze the circuit, we'll consider the two translinear loops:

\begin{center}
\includegraphics[width=5.5in]{gilbertcell-loops}
\end{center}

The TLP states that the product of clockwise currents in each loop must
equal the product of counterclockwise currents in the same loop:

\begin{align}\label{gc-s1}
    I(Q1) \cdot I(Q3) &= I(Q2) \cdot I(Q6)
&   I(Q1) \cdot I(Q5) &= I(Q4) \cdot I(Q6)
\end{align}

We know $I(Q1)$ and $I(Q6)$, since they're both inputs:

\begin{align}\label{gc-s2}
    (I_{BX} - I_X) \cdot I(Q3) &= (I_{BX} + I_X) \cdot I(Q2)
&   (I_{BX} - I_X) \cdot I(Q5) &= (I_{BX} + I_X) \cdot I(Q4)
\end{align}

The currents entering transistor pairs $[Q2, Q3]$ and $[Q4, Q5]$ combine
to give the $Y$ inputs $I_{BY}+I_Y$ and $I_{BY}-I_Y$:

\begin{align}\label{gc-s3}
    I_{BY} + I_Y &= I(Q2) + I(Q3)
&   I_{BY} - I_Y &= I(Q4) + I(Q5)
\end{align}

Using equation \eqref{gc-s2}, we can write each transistor in a pair in terms of the
other transistor in that pair. We'll rewrite \eqref{gc-s3} to give four equations,
one for each paired transistor:

\begin{equation}\label{gc-s4}
\begin{aligned}
    I_{BY} + I_Y &= I(Q2) + \left(\frac{I_{BX}+I_X}{I_{BX}-I_X}\right) \cdot I(Q2)
&   I_{BY} + I_Y &= I(Q3) + \left(\frac{I_{BX}-I_X}{I_{BX}+I_X}\right) \cdot I(Q3)
\\  I_{BY} - I_Y &= I(Q4) + \left(\frac{I_{BX}+I_X}{I_{BX}-I_X}\right) \cdot I(Q4)
&   I_{BY} - I_Y &= I(Q5) + \left(\frac{I_{BX}-I_X}{I_{BX}+I_X}\right) \cdot I(Q5)
\end{aligned}
\end{equation}

We'll solve each one for its transistor current and simplify:

\begin{equation}\label{gc-s5}
\begin{aligned}
    I(Q2) &= \frac{(I_{BY} + I_Y)(I_{BX} - I_X)}{2\cdot I_{BX}}
&   I(Q3) &= \frac{(I_{BY} + I_Y)(I_{BX} + I_X)}{2\cdot I_{BX}}
\\  I(Q4) &= \frac{(I_{BY} - I_Y)(I_{BX} - I_X)}{2\cdot I_{BX}}
&   I(Q5) &= \frac{(I_{BY} - I_Y)(I_{BX} + I_X)}{2\cdot I_{BX}}
\end{aligned}
\end{equation}

Now, each of the final output comprises the sum of the collector currents of the
transistors connected to that output:

\begin{align}\label{gc-s6}
    I_{OP} &= I(Q3) + I(Q4)
&   I_{ON} &= I(Q2) + I(Q5)
\end{align}

Simplifying each one:

\begin{equation}\label{gc-s7a}\begin{aligned}
    I_{OP} &= \frac{(I_{BY} + I_Y)(I_{BX} + I_X) + (I_{BY} - I_Y)(I_{BX} - I_X)}{2\cdot I_{BX}} \\
    I_{OP} &= \frac{2I_{BX}\cdot I_{BY} + 2I_X \cdot I_Y}{2\cdot I_{BX}} \\
    I_{OP} &= \frac{I_{BX} \cdot I_{BY} + I_X \cdot I_Y}{I_{BX}}
\end{aligned}\end{equation}
\begin{equation}\label{gc-s7b}\begin{aligned}
    I_{ON} &= \frac{(I_{BY} + I_Y)(I_{BX} - I_X) + (I_{BY} - I_Y)(I_{BX} + I_X)}{2\cdot I_{BX}} \\
    I_{ON} &= \frac{2I_{BX}\cdot I_{BY} - 2I_X \cdot I_Y}{2\cdot I_{BX}} \\
    I_{ON} &= \frac{I_{BX} \cdot I_{BY} - I_X \cdot I_Y}{I_{BX}}
\end{aligned}\end{equation}

Now, since the final output is meant to be $I_{OP} - I_{ON}$, we subtract and simplify:

\begin{equation}\label{gc-s8}
I_{OP} - I_{ON} = \frac{2}{I_{BX}} \left( I_X \cdot I_Y \right)
\end{equation}

\section{Design: Support circuitry}

\subsection{Current Reference}

The Gilbert cell requires two differential current inputs, so we will use a
quadruple current reference circuit to provide the common-mode current:

\begin{center}
\includegraphics[width=2.5in]{current-reference}
\end{center}

$V_B$ is a conveniently available bias voltage that will be used in the
next part.

\note{The $10\kOhm$ resistors were selected to keep a large voltage
    across the resistor, compared to the voltage across the corresponding
    base-emitter junction, at the $600\;\mr{\mu A}$ current. This means that
    variation in the transistors has a minimal effect on the current, and they
    likely won't need to be matched transistors. The three transistors at the
    bottom use $500\Ohm$ resistors because these transistors are inside
    an MC1496 modulator IC, where they already include these resistors. The
    transistors inside this chip are remarkably well matched, so there isn't
    much need for large resistors. }

\subsection{Voltage-to-current converter}

Now that the common-mode currents are generated, we will use two differential
voltage-to-current converters to give the input currents:

\begin{center}
\includegraphics[width=2.5in]{v2c}
\end{center}

The design of the circuit requires the $X$ section to sit at a higher voltage
than the $Y$ section (as $I_X$ is an input to transistors' collectors and $I_Y$
is an input to transistors' emitters), so we use the reference voltage $V_B$ to
bias $V_X$ up. The simple voltage-divider bias also effectively divides the
voltage by two; we'll compensate for this later when setting gains.

\subsection{Current-to-voltage converter}

In the end, we'd like to have an output voltage referenced to ground. The top
portion of the Gilbert cell already sits above ground and \emph{sinks} current,
making it incapable of feeding current down toward a ground-referenced
amplifier.

Instead of directly feeding its output to the output amplifier, we'll source
current to the output amplifier using resistors, and then use the Gilbert cell
to \emph{divert} current from them:

\begin{center}
\includegraphics[width=2.5in]{c2v}
\end{center}

The supply voltage is $V_B$ to ensure that the signals are always with the
op-amp's allowed input common-mode range.

Deriving a transfer function for this block is a simple Kirchhoff+Ohm
problem. The voltage at the op amp inputs has been labeled with a blue
$V$; remember that when an op amp is functioning correctly, its two
inputs will be very nearly equal.

This means that both R1 and R2 have the same voltage across them, and
their resistances are also equal; we'll call the equal current through them
$I_{COM}$.

\begin{equation}
I_{COM} = I(R1) = I(R2) = \frac{V_B - V}{R_{1}}
\end{equation}

The voltage across R3 is $V - V_O$:

\begin{equation}
V - V_O = I(R3) \cdot R3
\end{equation}

Now, $I(R3)$ must be the part of $I_{COM}$ that remains after the Gilbert
cell diverted $I_{OP}$ away from that node. Thus:

\begin{equation}\label{vr3}
V - V_O = (I_{COM} - I_{OP}) \cdot R3
\end{equation}

The voltage across R4 is $V$, and likewise, its current must be the part of
$I_{COM}$ that remains after the Gilbert cell diverted $I_{ON}$:

\begin{gather}\label{vr4}
    V = I(R4) \cdot R4 \\ \label{vr4b}
    V = (I_{COM} - I_{ON}) \cdot R4
\end{gather}

Combining equations \eqref{vr3} and \eqref{vr4b} yields this, which we can
solve for $V_O$:

\begin{gather}
    (I_{COM} - I_{ON}) \cdot R4 - V_O = (I_{COM} - I_{OP}) \cdot R3 \\
    V_O = (I_{COM} - I_{ON}) \cdot R4 - (I_{COM} - I_{OP}) \cdot R3
\end{gather}

Because R3 and R4 are equal in resistance:

\begin{gather}
    V_O = (I_{COM} - I_{ON}) \cdot R3 - (I_{COM} - I_{OP}) \cdot R3 \\
    \label{c2v-txf}
    V_O = (I_{OP} - I_{ON}) \cdot R3
\end{gather}

\subsection{Full circuit}
\begin{center}
\includegraphics[width=6in]{full}
\end{center}

The MC1496 ``Balanced Modulator'' IC is useful for our application: it contains
a partial Gilbert cell,
a low-side current mirror and the low-side voltage-to-current converter. We will
use an additional pair of matched NPN transistors from a CA3096 transistor array
to provide the missing transistors, and will also use a matched pair of PNP
transistors from the same array in the high-side voltage-to-current converter.
An LM7171 high speed op amp was selected for the current-to-voltage converter,
as this stage has high gain (giving a high gain-bandwidth product even at relatively
low frequencies).

The circuit is highly sensitive to mismatch between parts, so trimmers were
installed in four places:

\begin{enumerate}
    \item{Between the two halves of the high-side current reference, to adjust
        $I_{BX}$ to be equal on both halves of the differential circuit.}
    \item{As part of $R_{GX}$, to adjust the overall gain.}
    \item{Between the two $1\kOhm$ load resistors in the current-to-voltage
        converter, to balance them.}
    \item{As part of one of the two gain-setting resistors in the current-to-voltage
        converter, to null the common-mode voltage.}
\end{enumerate}

\emph{All} of these trimmers (with the exception of $R_{GX}$) affect the symmetry
of the output, making calibration a bit tedious. The calibration steps were:

\begin{enumerate}
    \item{Set inputs $V_X$ and $V_Y$ both to zero. Disconnect the Gilbert cell from
            the C-V converter, and adjust the load resistor trimmer
            to null the voltage across it. Reconnect the circuits.}
    \item{Adjust the C-V gain setting resistor to null the output voltage.}
    \item{Set input $V_X$ to a $2\Vpp$, $1\kHz$ sine wave and input $V_Y$ to a
            $2\Vpp$, $10\kHz$ sine wave, phase-locked with $0^\circ$ phase offset.
            Adjust the current reference trimmer for a symmetric output.}
    \item{Set input $V_X$ and $V_Y$ both to an accurate $1\;\mr V$. Adjust
            $R_{GX}$ for $1\;\mr V$ output.}
    \item{Repeat steps 1--4 until no adjustment has to be made.}
\end{enumerate}

\subsection{Transfer function}

The transfer function of the Gilbert cell itself is this:

\begin{equation}
I_{OP} - I_{ON} = \frac{2}{I_{BX}} \left( I_X \cdot I_Y \right)
\tag{\ref{gc-s8} revisited}
\end{equation}

We can now substitute the transfer function of the current-to-voltage converter.
Note that R3 was selected above to be $10\kOhm$,
and $I_{BX}$ was selected earlier to be $600\;\mr{\mu A}$:

\begin{gather}
V_O = (I_{OP} - I_{ON}) \cdot R3 \tag{\ref{c2v-txf} revisited} \\
V_O = \frac{2}{600\;\mr{\mu A}} \left( I_X \cdot I_Y \right) \cdot 10\kOhm
\end{gather}

Now, substituting the transfer functions of the voltage-to-current converters:

\begin{gather}
I_X = \frac{V_X}{2 R_{GX}} \\
I_Y = \frac{V_Y}{R_{GY}} \\
V_O = \left(\frac{2}{600\;\mr{\mu A}}\right) \left(\frac{V_X V_Y}{2 R_{GX} R_{GY}}\right) \cdot 10\kOhm
\end{gather}

\subsection{Setting the gains}

$R_{GY}$ was selected as $2\kOhm$. In order for the final coefficient to equal $1\;\mr{V}^{-1}$,
we can compute $R_{GX}$:

\begin{gather}
    V_O = \left(\frac{2}{600\;\mr{\mu A}}\right) \left(\frac{V_X V_Y}{2 (2\kOhm) R_{GX}}\right) \cdot 10\kOhm = k\cdot V_X \cdot V_Y \\
    k = \left(\frac{2}{600\;\mr{\mu A}}\right) \left(\frac{10\kOhm}{2 (2\kOhm) R_{GX}}\right) = 1\;\mr{V}^{-1} \\
    R_{GX} = 8.33\kOhm
\end{gather}


\section{Testing the circuit}

\subsection{Amplitude modulation demo}

The circuit was built on a breadboard and supplied with two signals:

\begin{gather}
V_A = (1\;\mr V) \sin(2\pi(10\;\mr{kHz})t) \\
V_B = (1\;\mr V) \sin(2\pi(1\;\mr{kHz})t)
\end{gather}


\begin{center}
\includegraphics[width=4in]{testing/am}
\end{center}


\subsection{Characterization}

A dual-channel function generator (generating DC) and a multimeter were scripted
to test the multiplier over its full range, at just over 1000 points. A quick plot
of the data shows that the device indeed works as a DC multiplier:

\begin{center}
    \includegraphics[width=4in]{testing/transfer}
\end{center}

Plotting one of the transfer curves with its linear fit shows that the multiplier
is quite accurate and linear:

\begin{center}
    \includegraphics[width=4in]{testing/transfer-a25}
\end{center}

The measurement setup used at the time was insufficient to properly characterize
the multiplier for linearity (error data was swamped by measurement noise). Further
attempts were not made to measure it because I plan to revisit this circuit later,
building a version with even tighter specifications --- more measurements will be
completed then.

\section{Conclusion}

The Gilbert cell multiplier, the base for most commercial analog multipliers, is
quite an accurate and usable circuit, if a bit complicated. This circuit can be
built entirely with parts that are still in production (the CA3096 is not, but
matched transistor pairs are still easy to find), and should be able to achieve
similar performance to commercial devices.

\end{document}
