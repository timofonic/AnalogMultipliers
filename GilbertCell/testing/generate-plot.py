from pylab import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
rc ('font', **{'family':'sans-serif', 'sans-serif':'Linux Biolinum', 'size':16})

def float_eq (a, b):
    return abs(a - b) < min (1e-3 * a, 1e-3 * b)

# InA, InB, out
data = []

with open ("test-dc-out") as f:
    for line in f:
        ina, inb, out = line.split ()
        ina = float (ina.rstrip(','))
        inb = float (inb.rstrip(','))
        out = float (out.rstrip(','))
        data.append ((ina, inb, out))

data.sort ()

#######################################
# Plot: output vs. InA over eight InBs
inbs = sorted (set (i[1] for i in data))
inbs_plot = inbs[::len(inbs)//7]
print (inbs_plot)
for inb in inbs_plot:
    plot_x = [i[0] for i in data if i[1] == inb]
    plot_y = [i[2] for i in data if i[1] == inb]

    # Generate a text label on the line
    # First, find the rotation angle for it
    coeffs = polyfit (plot_x, plot_y, 1)
    angle = arctan (coeffs[0]) * (180/pi) * 0.65

    # Now, the position
    text_x = 0.6
    text_y = plot_y[17*len(plot_y)//20] * 1.3
    if text_y >= -0.1:
        text_y += 0.025
    else:
        text_y /= 1.4
        text_y += 0.1
    print (text_y)

    text (text_x, text_y, 'InB=%.2f V' % inb, rotation=angle)
    plot (plot_x, plot_y, 'b')

xlabel ("Input A (V)")
ylabel ("Multiplier output (V)")
title ("Multiplier Transfer Characteristic")
grid ()
savefig ('transfer.eps')
show ()

#######################################
# Plot: output vs. InB when InA = 0.25
inbs = []
outs = []
for ina, inb, out in data:
    if ina > 0.24 and ina < 0.26:
        inbs.append (inb)
        outs.append (out)

coeffs = polyfit (inbs, outs, 1)
outs_ideal = [polyval (coeffs, i) for i in inbs]

plot (inbs, outs, 'r', label="Measured output")
plot (inbs, outs_ideal, 'b', label="Ideal output")
xlabel ("Input B (V)")
ylabel ("Multiplier output (V)")
title ("Transfer Curve and Ideal Response (Input A = 0.25V)")
grid ()
legend (loc=2)
savefig ('transfer-a25.eps')
show ()


############################################
# Plot: output vs. InB error when InA = 0.25
inbs = []
outs = []
for ina, inb, out in data:
    if ina > 0.24 and ina < 0.26:
        inbs.append (inb)
        outs.append (out)

coeffs = polyfit (inbs, outs, 1)
outs_ideal = [polyval (coeffs, i) for i in inbs]

errors = [(i - j) for i, j in zip (outs, outs_ideal)]
plot (inbs, errors)
xlabel ("Input B (V)")
ylabel ("Error")
title ("Error")
grid ()
savefig ('error.eps')
show ()


