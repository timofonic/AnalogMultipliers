#!/usr/bin/env python

import sys
import time
import random

sys.path.append ("/home/cmp/electronics/GPIB")
import lab.tds380
import lab.dg1022

# Connect DG1022 ch1 to input A, ch2 to input B, TDS380 ch1 to output

scope = lab.tds380.tds380 ()
siggen = lab.dg1022.dg1022 ()

def conf_scope (scope):
    scope.trigMode = 'NORMAL'
    scope.trigSource = 'EXT'
    scope.acqMode = 'SAMPLE'
    scope.acqNumAvg = 4
    scope.acqRun = False
    scope.acqSingle = True
    scope.ch1_coupling = 'DC'
    scope.ch1_scale = 2.5
    scope.horiz_scale = 0.001

def meas_dc (scope):
    measurements = []
    for i in range (4):
        scope.acqRun = True
        scope.trigger ()
        measurements.append ( scope.measure ('ch1', 'mean'))
    return sum (measurements) / len (measurements)

def conf_siggen (siggen):
    siggen.load (1, None)
    time.sleep (0.1)
    siggen.load (2, None)
    time.sleep (0.1)
    siggen.apply (1, 'dc', offs=0.0)
    time.sleep (0.1)
    siggen.apply (2, 'dc', offs=0.0)
    time.sleep (0.1)
    siggen.toggle (1, True)
    time.sleep (0.1)
    siggen.toggle (2, True)
    time.sleep (0.1)

conf_scope (scope)
conf_siggen (siggen)

inA_range = [i/16.0 for i in range (-16, 17)]
inB_range = [i/16.0 for i in range (-16, 17)]

test_coords = []
for a in inA_range:
    for b in inB_range:
        test_coords.append ((a, b))

# Shuffle list so thermal drift doesn't coordinate with data
random.shuffle (test_coords)


with open ('test-dc-out', 'w') as f:
    while test_coords:
        a, b = test_coords.pop ()

        
        expect = (a * b)
        scope.ch1_scale = max (abs(expect)/2, 0.075)
        siggen.apply (1, 'dc', offs=a)
        time.sleep (0.1)
        siggen.apply (2, 'dc', offs=b)
        time.sleep (0.1)
        meas = meas_dc (scope)

        # If this doesn't match our expectation, put it back in the
        # list and reshuffle
        if expect != 0. and abs ((meas - expect) / expect) > 0.25:
            test_coords.append ((a, b))
            random.shuffle (test_coords)
            print ("[%04d] %f, %f: RETRY (%f)" % (len (test_coords), a, b, meas))

        else:
            print ('[%04d] %f, %f, %f' % (len (test_coords), a, b, meas))
            f.write ('%f, %f, %f\n' % (a, b, meas))
            f.flush ()
